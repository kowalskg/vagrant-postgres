Test Postgres installation in Vagrant

Prerequesents that need to be installed on localhost : 

	- Ant
	- Vagrant
	- Virtual Box 

Then Run : 
# Create and Set up VM 
vagrant up
# Runs the build.xml file to create default database and set up users. 
ant

This starts a Postgres Instance in the VM and port forwards from 5432 on the VM to port 15432 on the localhost. 

Users vagrant and sets up a schema with access granted to this user. 

Then one can run : 

ant recreate.user.postgresql 

to  drop and re-create the user and all objects owned by that user. 
